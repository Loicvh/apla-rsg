import numpy as np
from eldp import Eldp
from eldp import Eldp_R
from gurobipy import *
import random
import getProblem
import quadprog
import copy
from cvxopt import matrix, sparse, spmatrix, solvers
import matplotlib.pyplot as plt
import time
import warnings


def eldp_subgrad(E):
    """
    
    Run the Riemannian Subgradient Descent (RSG)

    :param E: Eldp problem

    :return p: Best iterate 

    """
    param = E.param
    p = param.x0
    if not E.is_feasible(param.x0):
        print('Starting point is infeasible')
        p = get_feasible(E, param.x0) #TODO TO CHANGE
        #raise
    k = 0
    print('Initial', E.f(p))
    flag_S = False
    delta = 1
    cost_old = float('Inf')
    k_delta = 0
    while k < param.max_step and not flag_S and k_delta < 2: # and not E.stopping_criterion: # TODO TO CHANGE 200 > param.n_iter
        print('\n\nSubgradient iteration', k)
        E.get_this_S_np = E.get_S_np
        direction = sub_problem(E, p)

        S = E.get_S_np(p)
        direction_dic = to_dic(E, direction/np.linalg.norm(direction))
        
        E.update(p, S)
        
        p = get_step(direction, E, p)
        delta = cost_old - E.best_obj
        print('delta is', delta)
        if delta == 0.0:
            k_delta += 1
        else:
            k_delta = 0
            
        cost_old = E.best_obj
        if E.stopping_criterion:
            print('Stopping criterion is', E.stopping_criterion)
            step = 0
            E.update(p)
            new_direction = direction / np.linalg.norm(direction)
            new_direction = to_dic(E, new_direction)
            new_p = E.ell.retraction(p, {t: step*new_direction[t] for t in E.data.T})

            E.stopping_criterion = None
            _S = np.array(list(E.get_S_tight_np(new_p).values()))
            E.update(p)
            flag_S = True
            for s in _S:
                flag_S = flag_S and s.size >= E.data.n-1
        else:
            print('Normal S')
            #E.get_this_S_np = E.get_S_np

        k += 1  # End while loop on iteration

    if k == param.n_iter:
        E.stopping_criterion = 'Max number of iterations reached'
    if flag_S:
        E.stopping_criterion = 'Local optimal found'
    
    if E.stopping_criterion:
        print('Stopping criterion is', E.stopping_criterion)
    print('Feasibility', E.is_feasible(p))
    print('Loss is', E.get_loss(p))
    print('Obj is', E.f(p))
    p_np = np.array(list(p.values()))
    np.savetxt('p_sol.csv', p_np, delimiter=',')
    return p


def sub_problem(E, p):
    """ Solve subprolem to obtain descent direction

    Minimize     1/2 x^T G_qp x + a^T x

    Subject to   C_qp x <= b_qp
    """
    P_S = get_P_S(E, p)
    P_B = get_P_B(E, p)
    _, n1 = P_S.shape
    _, n2 = P_B.shape
    n = n1*len(E.data.T)
    P_b = get_P_b(E, p)
    A = np.hstack((P_S, P_B))
    G_qp = np_to_spmatrix(A.T @ A)
    a_qp = matrix(A.T @ P_b)
    C_qp_np = get_C_qp(n1, n2)
    C_qp = np_to_spmatrix(C_qp_np)
    b_qp_np = get_b_qp(n1, n2)
    b_qp = matrix(b_qp_np, tc='d')
    _C1 = np.eye(n1)
    C_qp = np_to_spmatrix(np.vstack((-_C1, _C1)))
    _b1 = np.ones(n1)
    b_qp = np_to_spmatrix(np.hstack((_b1, _b1)))
    C_qp = np_to_spmatrix(get_C_qp(n1, n2))
    b_qp = matrix(get_b_qp(n1, n2), tc='d')
    sol_qp = solvers.qp(G_qp, a_qp, C_qp, b_qp)
    x = sol_qp['x']
    x = np.reshape(np.array(x), (n1+n2))  # TODO ni -> n1+n2
    return -(P_b + A @ x)


def sub_problem_2(E, p):
    """ 

    Obtain the direction of violated constraint.
    This function is used to convert an infeasible point into a feasible point with param.flag_heuristic_feasibility = 'grad_violated_constraint'

    """
    direct =  {}
    data = E.data
    param = E.param
    for t_n in range(len(data.T)):
        t = data.T[t_n]
        direct[t] = np.zeros_like(p[t]) 
        for g in range(len(data.gen)):
            if p[t][g] < data.p_min_np[g]-param.eps_p:
                direct[t][g] = +1
            elif p[t][g] > data.p_max_np[g]+param.eps_p:
                direct[t][g] = -1
            if t_n > 0:
                X0 = p[data.T[t_n-1]][g]
                X1 = p[data.T[t_n]][g]
                if X1 - X0 > data.RU_np[g]+param.eps_p:
                    direct[data.T[t_n-1]][g] += 0.5
                    direct[data.T[t_n]][g] += -0.5
                elif - data.RD_np[g]-param.eps_p > X1 - X0:
                    direct[data.T[t_n-1]][g] += -0.5
                    direct[data.T[t_n]][g] += +0.5
    L =  list(direct.values())
    out = L[0]
    print(out)
    print(L)
    del L[0]
    for l in L:
        out = np.hstack((out, l))
    return out
        



def get_feasible(E, x=None):
    """
        Obtain a feasible point, if possible close to (infeasible) x.
    """
    data = E.data
    param = E.param
    var_old = E.var
    var = E.var
    alg_old = param.alg

    mod = getProblem.problem(E, param)
    param.alg = None
    if E.param.flag_verbose <= 1:
        mod.Params.LogToConsole = 0
    mod.Params.NonConvex = 2


    if x:
        for (t, g) in data.gen_T:
            E.var.p[t, g].Start = x[t][data.gen_dic[g]]
    mod.update()

    for (t, b) in data.bus_T:
        mod.remove(mod.getConstrByName('FlowConservation[%s,%s]' % (t, b)))
    if param.feasibility_obj != 'balance_deviation':
        for t in data.T:
            mod.addConstr(0 ==
                quicksum(
                    E.ell.A_full[t][i, j]*var.p[t, data.gen[i]]*var.p[t, data.gen[j]]
                    for i in range(data.n) for j in range(data.n))
                + quicksum(
                    E.ell.b_full[t][i]*var.p[t, data.gen[i]]
                    for i in range(data.n))
                + E.ell.c_full[t], name='FlowConservation[%s]' % t)

    if param.flag_spinning_reserve:
        set_spinning_reserve(E, mod)
    
    if x and param.feasibility_obj == 'balance_deviation':
        print('balance_deviation')
        mod.Params.NonConvex = 2
        alpha = mod.addVars(data.T, name='alpha')
        beta = mod.addVars(data.T, name='beta')
        param_a = 1/E.get_loss(x)
        param_b = 0*1/E.f(x)
        for t in data.T:
            mod.addConstr(alpha[t] ==
                quicksum(
                    E.ell.A_full[t][i, j]*var.p[t, data.gen[i]]*var.p[t, data.gen[j]]
                    for i in range(data.n) for j in range(data.n))
                + quicksum(
                    E.ell.b_full[t][i]*var.p[t, data.gen[i]]
                    for i in range(data.n))
                + E.ell.c_full[t], name='FlowConservation[%s]' % t)
            mod.setObjective(param_a*quicksum((alpha[t]*alpha[t]) for t in data.T)
                             +param_b*quicksum(
                                 (data.gen_fun_param[g][0]*var.p[t, g]*var.p[t, g]
                                  + data.gen_fun_param[g][1]*var.p[t, g]
                                  + data.gen_fun_param[g][2])
                                 for (t, g) in data.gen_T))

    if x is None:
        mod.setObjective(quicksum(
                             (data.gen_fun_param[g][0]*var.p[t, g]*var.p[t, g]
                              + data.gen_fun_param[g][1]*var.p[t, g]
                              + data.gen_fun_param[g][2])
                             for (t, g) in data.gen_T),
                         GRB.MINIMIZE)

    if x and param.feasibility_obj == 'norm':
        mod.setObjective(quicksum((var.p[t, g] - x[t][data.gen_dic[g]])*(var.p[t, g] - x[t][data.gen_dic[g]]) for g in data.gen for t in data.T))#+0*quicksum(

    if x and param.feasibility_obj == 'mixed_norm_quadratic': # BestONE
        mod.setObjective(quicksum((var.p[t, g] - x[t][data.gen_dic[g]])*(var.p[t, g] - x[t][data.gen_dic[g]]) for g in data.gen for t in data.T)+param.feasibility_mixed_cons*quicksum(
                             (data.gen_fun_param[g][0]*var.p[t, g]*var.p[t, g]
                              + data.gen_fun_param[g][1]*var.p[t, g]
                              + data.gen_fun_param[g][2])
                             for (t, g) in data.gen_T))

    if x and param.feasibility_obj == 'MILP': # 
        cumSum = 0
        for g in data.gen_VPE:
            if len(E.xi[data.T[0], g]) == 1:
                cumSum += E.f(data.p_min[g], data.T[0], g)
        mod.setObjective(
            len(data.T)*cumSum
            + quicksum(
                (data.gen_fun_param[g][0]*var.p[t, g]*var.p[t, g]
                 + data.gen_fun_param[g][1]*var.p[t, g]
                 + data.gen_fun_param[g][2])
                for (t, g) in data.gen_T if g not in data.gen_VPE),
            GRB.MINIMIZE)
        # E.param.alg = 'APLA'
        getProblem.updatePWL(E, mod, param)
    if x and param.feasibility_obj == 'losses':
        mod.setObjective(quicksum((
            quicksum(
                E.ell.A[t][i, j]*var.p[t, data.gen[i]]*var.p[t, data.gen[j]]
                for i in range(data.n) for j in range(data.n))
            + quicksum(
                E.ell.b[t][i]*var.p[t, data.gen[i]]
                for i in range(data.n))
            + E.ell.c[t])
            for t in data.T))
    
    if x and param.feasibility_obj == 'mixed_losses_quadratic':
        param_loss = E.get_loss(x)
        param_quad = E.f(x)
        print(param_loss, param_quad)
        mod.setObjective(1/param_loss*quicksum((
            quicksum(
                E.ell.A[t][i, j]*var.p[t, data.gen[i]]*var.p[t, data.gen[j]]
                for i in range(data.n) for j in range(data.n))
            + quicksum(
                E.ell.b[t][i]*var.p[t, data.gen[i]]
                for i in range(data.n))
            + E.ell.c[t])
            for t in data.T)
            + 1/param_quad*quicksum(
                (data.gen_fun_param[g][0]*var.p[t, g]*var.p[t, g]
                 + data.gen_fun_param[g][1]*var.p[t, g]
                 + data.gen_fun_param[g][2])
                for (t, g) in data.gen_T))


    mod.Params.SolutionLimit = 50
    mod.Params.timeLimit = 60
    if param.feasibility_obj == 'balance_deviation':
        mod.Params.SolutionLimit = 10000
    mod.update()
    mod.write('FeasibilityModel.lp')
    print('Optimizing feasible')
    param.alg = 'APLA'
    param.n_iter = 1
    mod.optimize()
    E.best_sol = mod.getAttr('x', var.p) 
    if param.flag_spinning_reserve:
        E.sr_tmp = mod.getAttr('Xn', var.sr)
    #check_incumbents(E, mod)
    p_out = E.gurobi_sol_to_np(E.best_sol)
    print(E.f(E.best_sol))

    E.var = var_old
    param.alg = alg_old
    _feas = E.is_feasible(p_out)
    if not _feas:
        warnings.warn('get_feasible did not produce feasible point')

    if param.flag_verbose >= 1:
        print('objective Value is', E.f(p_out))
    return p_out


def get_feasible_t(E, t):
    """
        Find a point t-feasible in solving (F)_t
    """
    print('Finding feasible point for t=', t)
    data = E.data
    param = E.param
    mod = Model()
    mod.Params.NonConvex = 2
    mod.Params.timeLimit = 20
    mod.Params.SolutionLimit = 1
    if E.param.flag_verbose <= 1:
        mod.Params.LogToConsole = 0
    var_p = mod.addVars(data.gen, lb=data.p_min, ub=data.p_max, name='p')
    coeff = {}
    for g in data.gen:
        if data.p_max[g] != data.p_min[g]:
            coeff[g] = 1.0/(data.p_max[g] - data.p_min[g])
        else:
            coeff[g] = 0.0
    if param.obj_feasible_t == 'mid':
        mod.addConstr(0 ==
            quicksum(
                E.ell.A_full[t][i, j]*var_p[data.gen[i]]*var_p[data.gen[j]]
                for i in range(data.n) for j in range(data.n))
            + quicksum(
                E.ell.b_full[t][i]*var_p[data.gen[i]]
                for i in range(data.n))
            + E.ell.c_full[t])
        mod.setObjective(quicksum(coeff[g]*
                             (var_p[g]-data.p_min[g]/2-data.p_max[g]/2) *
                             (var_p[g]-data.p_min[g]/2-data.p_max[g]/2)
                             for g in data.gen),
                         GRB.MINIMIZE)
    elif param.obj_feasible_t == 'losses' :
        mod.setObjective(
            quicksum(
                E.ell.A_full[t][i, j]*var_p[data.gen[i]]*var_p[data.gen[j]] *var_p[data.gen[i]]
                for i in range(data.n) for j in range(data.n))
            + quicksum(
                E.ell.b_full[t][i]*var_p[data.gen[i]]
                for i in range(data.n))
            + E.ell.c_full[t])
    else:
        raise ValueError('Illegal parameter obj_feasible_t', param.obj_feasible_t)

    mod.update()
    mod.optimize()
    losses = mod.getAttr('ObjVal')
    print('this loss = ', losses)
    _sol = mod.getAttr('X', var_p)
    x_t = [np.array([_sol[g] for g in data.gen])]
    
    for e in range(1, mod.SolCount):
        mod.setParam(GRB.Param.SolutionNumber, e)
        _sol = mod.getAttr('Xn', var_p)
        x_t.append(np.array([_sol[g] for g in data.gen]))

    return x_t


def get_feasible_pseudo(E):
    """
        Get a point p such that p_t is feasible for timestep t but the ramping constraint of p can be violated.
    """
    data = E.data
    p_out = {}
    for t in data.T:
        p_out[t] = get_feasible_t(E, t)
    return p_out


def check_incumbents(E, mod):

    """
        Check which incumbent gives the best (true) objective.
    """

    mod.setParam(GRB.Param.SolutionNumber, 0)
    _sol = mod.getAttr('Xn', E.var.p)
    if E.param.flag_warm_start:
        E.sr_tmp = mod.getAttr('Xn', E.var.sr)
    E.update_best_sol(_sol, forced=True)
    for e in range(1, mod.SolCount):
        mod.setParam(GRB.Param.SolutionNumber, e)
        _sol = mod.getAttr('Xn', E.var.p)
        if E.param.flag_warm_start:
            E.sr_tmp = mod.getAttr('Xn', E.var.sr)
        E.update_best_sol(_sol)



def get_b_qp(n1, n2):
    _b1 = np.ones(n1)
    _b2 = np.zeros(n2)
    return np.hstack((_b1, _b1, _b2))


def get_C_qp(n1, n2):
    _C1 = np.hstack((np.eye(n1), np.zeros((n1, n2))))
    _C2 = np.hstack((np.zeros((n2, n1)), np.eye(n2)))
    return np.vstack((-_C1, _C1, -_C2))


def get_P_B(E, p):
    data = E.data
    param = E.param
    if param.flag_spinning_reserve:
        sr = E.best_sr_np
        B = {t: np.zeros((data.n, 6*data.n)) for t in data.T}
        P_B = np.zeros((data.n*len(data.T), 6*data.n*len(data.T)))
    else:
        B = {t: np.zeros((data.n, 4*data.n)) for t in data.T}
        P_B = np.zeros((data.n*len(data.T), 4*data.n*len(data.T)))
    for t in data.T:
        for i in range(data.n):
            if E.is_tight_p_min(i, t, p):
                B[t][i, i] = -1
            elif E.is_tight_p_max(i, t, p):
                B[t][i, i+data.n] = 1
            if t is not data.T[0]:
                t1 = E.data.T[E.data.T_dic[t]-1]
                t2 = t
                if E.is_tight_RU(i, t, p):
                    B[t1][i, i+2*data.n] = 1
                    B[t2][i, i+2*data.n] = -1
                elif E.is_tight_RD(i, t, p):
                    B[t1][i, i+3*data.n] = -1
                    B[t2][i, i+3*data.n] = 1
        if param.flag_spinning_reserve:
            if E.is_tight_S_60(t, p):
                for i in range(data.n):
                    if data.p_max_np[i]-p[t][i] <= data.RU_np[i]:
                        B[t][i, i+4*data.n] = -1
        if param.flag_spinning_reserve:
            if E.is_tight_S_balance(t, sr):
                B[t][data.n:, 5*data.n:] = np.ones(data.n)
    t_num = 0
    for t in data.T:
        for j in range(4*data.n):
            B[t][0:data.n, j] = E.ell.projection(p[t], B[t][0:data.n, j], t)
        if param.flag_spinning_reserve:
            P_B[t_num*data.n:(t_num+1)*data.n, t_num*6*data.n:(t_num+1)*6*data.n] = B[t]
        else:
            P_B[t_num*data.n:(t_num+1)*data.n, t_num*4*data.n:(t_num+1)*4*data.n] = B[t]
        t_num += 1
    # Find and delete all 0 columns
    idx = np.argwhere(np.all(P_B[..., :] == 0, axis=0))
    return np.delete(P_B, idx, axis=1)
    return P_B


def get_P_b(E, p):
    data = E.data
    P_b = np.empty(0)
    E.update(p)
    subgrad = E.grad(p)
    for t in data.T:
        if isinstance(subgrad[t], np.ndarray):
            P_b_t = subgrad[t]
            print(P_b_t)
            print(E.f(p[t], t))
            p_test = p[t] - P_b_t*0.0001
            print(E.f(p_test, t))
            p_test = p[t] + P_b_t*0.0001
            print(E.f(p_test, t))

        else:
            P_b_t = E.ell.projection(p[t], subgrad[t].b, t)
        P_b = np.hstack((P_b, P_b_t))
    if E.param.flag_spinning_reserve:
        return P_b
        #return np.hstack((P_b, np.zeros_like(P_b)))
    else:
        return P_b


def get_P_S(E, p):
    data = E.data
    E.ell.update_ellipsoid(p)
    subgrad = E.grad(p)
    P_S = np.empty((0, data.n))
    for t in data.T:
        _P_S = np.zeros((data.n, data.n))
        if not E.ell.check_ellipsoid(p[t], t):
            warnings.warn("Warning: computing projection on non-admissible point")
        if not isinstance(subgrad[t], np.ndarray):
            _P_S[:, E.S_np[t]] = E.ell.projection(p[t], subgrad[t].A[:, E.S_np[t]], t)
        P_S = np.vstack((P_S, _P_S))
    if E.param.flag_spinning_reserve:
        return P_S
       # return np.vstack((P_S, np.zeros_like(P_S)))
    else:
        return P_S


def get_step(direction, E, p):
    """
        Find the direction and try to obtain the step (using armijo) via the retraction.

        :return new_p: The next iterate
    """
    cost = E.f(p)
    param = E.param
    step = E.step
    nd = np.linalg.norm(direction)
    E.update(p)
    S_old = copy.deepcopy(E.S_np)
    S_test = E.get_S_direction_np(p, direction)
    flag_frozen = False
    if E.param.flag_verbose >= 1:
        print('Direction norm is', nd)
    direction_np = direction
    direction = direction / nd
    direction = to_dic(E, direction)
    if param.step_forward > 0:
        step = step/param.step_scaling**(1+param.step_forward)
    else:
        step = param.step_0
    armijo = False
    count = 0
    if not E.is_feasible(p):
        warnings.warn("Looks like the starting point of this iteration is not feasible")
    new_p = p
    n_frozen = 0
    while not armijo:
        count += 1
        print('flag_frozen is', flag_frozen)
        if count == param.step_count_max*2:
            E.stopping_criterion = 'line search failed: maximum number of\
                    steps reached without improvement'
            return p
        elif step <= param.step_min:
            E.stopping_criterion = 'minimum step reached'
            return p
        if flag_frozen:
            n_frozen += 1
            S_new = E.get_S_tight_np(new_p)
            S_tot = {t: np.union1d(S_new[t], S_tot[t]) for t in E.data.T}
            #S_tot = {t: np.union1d(E.S_np[t], S_tot[t]) for t in E.data.T}
            print('Flag_frozen =', flag_frozen)
            E.is_feasible(new_p)
            E.S_np = S_tot
        else:
            S_tot = E.S_np
            step = param.step_scaling*step
        if E.param.flag_verbose >= 2:
            print('step is', step)

        p_test = {t: p[t]+0.001*direction[t] for t in E.data.T}
        E.update(p, E.S_np)
        for t in E.data.T:
            direction[t][S_tot[t]] = 0
            direction[t] = direction[t]/np.linalg.norm(direction[t])
        new_p = E.ell.retraction(p, {t: step*direction[t] for t in E.data.T})
        print('After retraction', E.f(new_p))
        if E.is_feasible(p_test):
            new_p = p_test
        flag_frozen = False
        print('Obj test is', E.f(new_p))
        print('Feasibility')
        if E.is_feasible(new_p):
            new_cost = E.f(new_p)
            armijo = new_cost < (cost - param.armijo_coef*step*nd)
            if n_frozen == 3:
                print('Reset frozeon')
                flag_frozen = False
                n_frozen = 0
            if E.param.flag_verbose >= 2:
                print('Testing armijo with cost = %s and new cost = %s returns %s'\
                      % (cost, new_cost, armijo))
            if armijo:
                E.best_sol = new_p
                E.best_obj = new_cost
                n_frozen = 0
                flag_frozen = False
        
        else:
            if n_frozen == 3:
                print('Reset frozeon')
                flag_frozen = False
                n_frozen = 0
    return new_p


def get_approx(E):
    """
        APLA part - Solve the relaxation of (P) via an sequence of adaptive PWL approximation, and return the best point along with a lower bound to the global solution of (P). The returned point is not feasible for (P)due to the relaxation of the power losses.

    """
    data = E.data
    param = E.param
    
    if param.flag_verbose >= 1:
        print('computing approximation')
        print('Start plane approximation')
    b_lower, b_upper, c_lower, c_upper = get_approx_planes(E)
    if param.flag_verbose >= 1:
        print('End plane approximation')
    mod = getProblem.problem(E, param)
    var = E.var
    mod.Params.timeLimit = E.param.timeLimit
    mod.Params.NonConvex = 2
    for (t, b) in data.bus_T:
        mod.write('thistest.lp')
        mod.remove(mod.getConstrByName('FlowConservation[%s,%s]' % (t, b)))
        mod.addConstr(0 <= c_lower[t] +
                      quicksum(b_lower[t][i] * var.p[t, data.gen[i]] for i in range(data.n)),
                      name='LowerFlowConserv[%s,%s]' % (t, b))
        mod.addConstr(0 >= c_upper[t] +
                      quicksum(b_upper[t][i] * var.p[t, data.gen[i]] for i in range(data.n)),
                      name='UpperFlowConserv[%s,%s]' % (t, b))
    if param.flag_spinning_reserve:
        set_spinning_reserve(E, mod)
    mod.update()
    param.flag_warm_start = False
    param.n_iter = 1
    mod.Params.mipgap = 0.001
    print('Solving via', param.alg)
    getProblem.solve_problem(E, mod, param)
    if E.param.flag_bound_tightening:
        mod = E.bound_tightening(mod)
        E.param.timeLimit = 60
        mod = getProblem.problem(E, param)
        var = E.var
        mod.write('todelete.lp')
        mod.Params.timeLimit = E.param.timeLimit
        mod.Params.NonConvex = 2
        for (t, b) in data.bus_T:
            mod.remove(mod.getConstrByName('FlowConservation[%s,%s]' % (t, b)))
            mod.addConstr(0 <= c_lower[t] +
                          quicksum(b_lower[t][i] * var.p[t, data.gen[i]] for i in range(data.n)),
                          name='LowerFlowConserv[%s,%s]' % (t, b))
            mod.addConstr(0 >= c_upper[t] +
                          quicksum(b_upper[t][i] * var.p[t, data.gen[i]] for i in range(data.n)),
                          name='UpperFlowConserv[%s,%s]' % (t, b))
        if param.flag_spinning_reserve:
            set_spinning_reserve(E, mod)
        mod.update()
        mod.write('wtf.lp')
    
        getProblem.solve_problem(E, mod, param)

    print('Time limit is', E.param.timeLimit)
    E.param.timeLimit = 600
    print('Convert solution into a feasible one')
    print('Loss is', E.get_loss(E.gurobi_sol_to_np(E.best_sol)))
    print('Balane deviation is', E.balance(E.gurobi_sol_to_np(E.best_sol)))
    x_APLA = E.full_dic_to_np_dic(E.best_sol)
    x0 = E.gurobi_sol_to_np(E.best_sol) #TODO TO CHANGE
    
    if param.flag_verbose >= 1:
        feas = E.is_feasible(x0)
        new_x = get_heuristic_3(E, x0)


        count = 0
        while not feas and count < 10000:
            if param.flag_heuristic_feasibility == 'grad_violated_constraint':
                direction = sub_problem_2(E, new_x)
                nd = np.linalg.norm(direction)
                direction_nd = direction / nd
                direction_np = to_dic(E, direction_nd)
                step = 0.1 *(0.999**count)
                new_x = get_heuristic_3(E, {t: new_x[t] + step*direction_np[t] for t in data.T})
            elif param.flag_heuristic_feasibility == 'box_projection':
                new_x = project_box(E, new_x)
                new_x = get_heuristic_3(E, {t: new_x[t]  for t in data.T})
                assert E.balance(new_x) < 0.1 # TODO TO CHANGE
            else:
                return get_feasible(E)
            feas = E.is_feasible(new_x)
            count += 1
        if feas:
            print('Feasible !')
            print(new_x)
            print('Loss is', E.get_loss(new_x))
            print('Deviation is', E.balance(new_x))
            print('Obj is', E.f(new_x))
            print('Initial objective = ', E.f(E.best_sol))
            p_np = np.array(list(new_x.values()))
            np.savetxt('p_sol_heur.csv', p_np, delimiter=',')
            




    print('Loss is', E.get_loss(x0))
    return new_x

def get_heuristic_3(E, x):
    """
        Map any point x to the quadric E.ellipsoid by looking at the closest intersection between the quadric and the segment [d, x] with d the quadric center.
    """
    param = E.param
    data = E.data
    b = data.B_loss_0 - 1
    d = np.linalg.solve(data.B_loss, -data.B_loss_0/2.0)
    q = {}
    for t in data.T:
        a1 = (x[t]-d).T @ data.B_loss @ (x[t]-d)
        a2 = 2*d.T @ data.B_loss @ (x[t]-d) + b.T @ (x[t]-d)
        a3 = d.T @ data.B_loss @ d + b.T @ d + (data.B_loss_00 + data.sum_loads[t])
        delta = a2**2 - 4*a1*a3
        if delta < 0:
            raise ValueError('Negative delta, it seems impossible to find intersections\
                             between (sub-)ellipsoid and the line between x and centre')
        betas = (-a2 + np.array([1, -1]).T * np.sqrt(delta).T) / (2.0*a1)
        ind = np.argmin(np.abs(betas-1))
        q[t] = d + betas[ind] * (x[t] - d)
        assert q[t].T @ data.B_loss @ q[t] + b.T @ q[t] + (data.B_loss_00 + data.sum_loads[t]) <= 0.001, 'Error'
    return q 


def get_approx_planes(E):
    """
        Find the up and down planes to relax the quadratic constraint as the intersection of the two half spaces defined by the planes.
    """
    data = E.data
    param = E.param
    param.x0 = None
    L, _ = np.linalg.eig(data.B_loss)
    E.is_ellipsoid = np.all(L >= 0)
    if E.param.alg_feasible_approx == 'local':
        x0 = get_feasible_pseudo(E)
    else:
        x0 = get_feasible(E)
        warnings.warn('Are you sure that you want the first point to be globally feasible?')
        assert E.is_feasible(x0)
    b_lower = {}
    c_lower = {}
    b_upper = {}
    c_upper = {}
    for t in data.T:
        _b_lower, _b_upper, _c_lower, _c_upper = get_approx_planes_t(E, t, x0)
        b_lower[t] =  _b_lower
        b_upper[t] = _b_upper
        c_lower[t] = _c_lower
        c_upper[t] = _c_upper
    return b_lower, b_upper, c_lower, c_upper


def get_approx_planes_t(E, t, x0):
    """
        Get the approximation planes for a given time index t.
    """
    data = E.data
    param = E.param
    print('\n Getting approximate plane for t=', t, '\n')
    mod = Model()
    mod.Params.NonConvex = 2
    mod.Params.timeLimit = 30
    mod.Params.mipgap = 5
    data = E.data
    var = E.var
    var.p = mod.addVars(data.gen, lb=data.p_min, ub=data.p_max, name='p')
    for b in data.bus:
        mod.addConstr(0 ==
            quicksum(
             E.ell.A_full[t][i, j]*var.p[data.gen[i]]*var.p[data.gen[j]]
                for i in range(data.n) for j in range(data.n))
            + quicksum(
                E.ell.b_full[t][i]*var.p[data.gen[i]]
                for i in range(data.n))
            + E.ell.c_full[t], name='FlowConservation[%s,%s]' % (t, b))

    if param.flag_spinning_reserve:
        set_spinning_reserve_t(E, mod, t, var.p)
    n = E.ell.A_full[t] @ x0[t][0] + E.ell.b_full[t]  # Normal is the constraint gradient 
    n_normed = n / np.linalg.norm(n)  # Opposite sign since we want the inner normal
    mod.setObjective(quicksum(n_normed[data.gen_dic[g]]*(var.p[g]-x0[t][0][data.gen_dic[g]])
                              for g in data.gen), GRB.MAXIMIZE)
    mod.update()
    if param.check_feasibility:
        print('check feasibility')
        check_feasibility_reserve_t(E, t, x0)
    mod.optimize()
    x = mod.getAttr('x', var.p)
    x = np.array(list(x.values()))
    scalar_product = mod.getAttr('ObjVal')
    b_lower = n
    c_lower = -b_lower @ x0[t][0]
    b_upper = n
    y_upper = x0[t][0] + n_normed * scalar_product
    c_upper = - n.T @ y_upper
    # Could be simplified since n.T @ n_normed = 1
    # assert abs(np.dot(b_upper, x) + c_upper) < param.eps_p
    if E.param.flag_verbose >= 2:
        print('Scalar product = % s' % scalar_product)
    
    if not E.is_ellipsoid:
        mod.setObjective(-quicksum(n_normed[data.gen_dic[g]]*(var.p[g]-x0[t][0][data.gen_dic[g]])
                              for g in data.gen), GRB.MAXIMIZE)
        mod.update()
        mod.optimize()
        x = mod.getAttr('x', var.p)
        x = np.array(list(x.values()))
        scalar_product = mod.getAttr('ObjVal')
        b_lower = n
        y_lower = x0[t][0] - n_normed*scalar_product
        c_lower = - n.T @ y_lower


    return b_lower, b_upper, c_lower, c_upper


def project_box(E, x):  # TODO: Check also reserve
    """
        Project a given point x inside the polytope. In general the point will not be on the quadric anymore.
    """
    data = E.data
    param = E.param
    mod = getProblem.problem(E, param)
    var = E.var
    mod.write('test_projection.lp')
    print(data.bus_T)
    for (t, b) in data.bus_T:
        mod.remove(mod.getConstrByName('FlowConservation[%s,%s]' % (t, b)))
    
    mod.setObjective(quicksum((var.p[t, g] - x[t][data.gen_dic[g]])*(var.p[t, g] - x[t][data.gen_dic[g]]) for g in data.gen for t in data.T))
    mod.optimize()
    _x = mod.getAttr('x', var.p) 
    #check_incumbents(E, mod)
    x_out = E.gurobi_sol_to_np(_x)
    return x_out



def check_feasibility_reserve_t(E, t, x0):
    """
       Check that the reserve are not violated. 
    """
    data = E.data
    param = E.param
    mod = Model()
    mod.Params.NonConvex = 2
    mod.Params.timeLimit = 10
    mod.Params.mipgap = param.mipgap
    data = E.data
    var = E.var
    p = mod.addVars(data.gen, lb=data.p_min, ub=data.p_max, name='p')



    if param.flag_spinning_reserve:
        set_spinning_reserve_t(E, mod, t, p)
    mod.Params.mipgap = 0.001
    mod.update()
    mod.write('feasibility_t.lp')
    mod.optimize()
    x = mod.getAttr('x', p)
    x = np.array(list(x.values()))
    scalar_product = mod.getAttr('ObjVal')
    if E.param.flag_verbose >= 2:
        print('Scalar product = % s' % scalar_product)
    if np.sum(data.p_max_np) <= data.D_tot[t, data.bus[0]] + mod.ObjBound + data.SR[t]:
        raise
        warnings.warn('Looks like non-feasible problem')


def np_to_spmatrix(M):
    idx = M.nonzero()
    return matrix(M, tc='d') # if one wants to use dense matrices
    return spmatrix(M[idx], idx[0], idx[1], M.shape, tc='d')

def set_spinning_reserve(E, mod):
    data = E.data
    var = E.var
    RU_T = {(t, g): data.RU[g] for (t, g) in data.gen_T}
    RU_T_10 = {(t, g): data.RU[g]/6 for (t, g) in data.gen_T}
    var.sr = mod.addVars(data.gen_T, lb={(t, g): 0 for (t, g) in data.gen_T}, ub=RU_T)
    var.sr_10 = mod.addVars(data.gen_T, lb={(t, g): 0 for (t, g) in data.gen_T}, ub=RU_T_10)
    var.min_foo = mod.addVars(data.gen_T, lb={(t, g): 0 for (t, g) in data.gen_T},
                              ub={(t, g): data.p_max[g] - data.p_min[g]
                                  for (t, g) in data.gen_T})
    for (t, g) in data.gen_T:
        mod.addConstr(var.min_foo[t, g] == data.p_max[g] - var.p[t, g])
        mod.addConstr(var.sr[t, g] == min_(var.min_foo[t, g], data.RU[g]))
        mod.addConstr(var.sr_10[t, g] == min_(var.min_foo[t, g], data.RU[g]/6))
    for t in data.T:
        mod.addConstr(quicksum(var.sr[t, g] for g in data.gen) >= data.SR[t], name='SpinningBalance')
        mod.addConstr(quicksum(var.sr_10[t, g] for g in data.gen) >= data.SR_10[t], name='SpinningBalance')


def set_spinning_reserve_t(E, mod, t, p):
    data = E.data
    var = E.var
    var.sr = mod.addVars(data.gen, lb={g: 0 for g in data.gen}, ub=data.RU)
    var.sr_10 = mod.addVars(data.gen, lb={g: 0 for g in data.gen}, ub=data.RU)
    var.min_foo = mod.addVars(data.gen, lb={g: 0 for g in data.gen},
                              ub={g: data.p_max[g] - data.p_min[g] for g in data.gen})
    for g in data.gen:
        mod.addConstr(var.min_foo[g] == data.p_max[g] - p[g])
        mod.addConstr(var.sr[g] == min_(var.min_foo[g], data.RU[g]))
        mod.addConstr(var.sr_10[g] == min_(var.min_foo[g], data.RU[g]/6))
    mod.addConstr(quicksum(var.sr[g] for g in data.gen) >= data.SR[t],
                  name='SpinningBalance')
    mod.addConstr(quicksum(var.sr_10[g] for g in data.gen) >= data.SR_10[t],
                  name='SpinningBalance10')

def to_dic(E, x):
    # 
    data = E.data
    x_dic = {}
    t_num = 0
    for t in data.T:
        x_dic[t] = x[t_num*data.n:(t_num+1)*data.n]
        t_num += 1
    return x_dic


