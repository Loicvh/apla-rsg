import csv
from numpy import genfromtxt
import numpy as np
import math
import matplotlib.pyplot as plt
import copy
import sys
import os

import duplicate_vpe
import random

sys.path.insert(0, "/home/loicvh/Documents/Phd_doc/APLA-RiemannianSG-Clean/samples")  # TODO Hardcoded
import getData

random.seed(42)

root_dir, _ = os.path.split(os.path.dirname(os.path.abspath(__file__)))
str_path = os.path.join(root_dir, 'data/matpower/')
end_str_bus = 'bus.csv'
end_str_branch = 'branch.csv'
end_str_gen = 'gen.csv'
end_str_gencost = 'gencost.csv' 
str_path_VPE = os.path.join(root_dir, 'data/vpe/')
str_path_data = os.path.join(root_dir, 'data/')
str_path_file = os.path.join(root_dir, 'data/tmp/')


def writeDataMatpower(T, RD, RU, flag_VPE, flag_multiple_generator):
    bus = genfromtxt(str_path+end_str_bus, delimiter=',')
    branch = genfromtxt(str_path+end_str_branch, delimiter=',')
    gen = genfromtxt(str_path+end_str_gen, delimiter=',')
    gencost = genfromtxt(str_path+end_str_gencost, delimiter=',')
    n_bus = len(bus)
    (n_gen, _) = gencost.shape
    (n_line, _) = branch.shape

    print('Number of buses is', n_bus)
    print('Number of generators is', n_gen)
    print('Number of lines is', n_line)
    if flag_VPE:
        gencost_VPE = genfromtxt(str_path_VPE + end_str_gencost, delimiter=',')
        (n_gen_VPE, _) = gencost_VPE.shape
        print('Number of VPE generators is', n_gen_VPE)
        if flag_multiple_generator:
            bus_VPE = copy.copy(bus[:, 0])
            np.random.shuffle(bus_VPE)
            bus_VPE = bus_VPE[0:n_gen_VPE]
        else:
            bus_without_gen = np.array([x for x in bus[:, 0] if x not in gen[:, 0]])
            np.random.shuffle(bus_without_gen)
            bus_VPE = bus_without_gen[0:n_gen_VPE]


    # Creating Buses.csv
    with open(str_path_file+'/'+'Buses.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(1, n_bus+1):
            writer.writerow(['B%s' % i])

    with open(str_path_file+'/'+'BusGenerators.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_gen):
            writer.writerow(['G%s' % (i+1), 'B%s' % int(gen[i, 0])])
        if flag_VPE:
            for i in range(0, n_gen_VPE):
                writer.writerow(['G%s' % (i+1+n_gen), 'B%s' % int(bus_VPE[i])])


    # Creating BusLoads.csv
    with open(str_path_file+'/'+'BusLoads.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_bus):
            writer.writerow(['LD%s' % (i+1), 'B%s' % (i+1)])

    createDemand(n_bus, bus, T)

    with open(str_path_file+'/'+'FromBus.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_line):
            writer.writerow(['L%s' % (i+1), 'B%s' % int(branch[i, 0])])

    with open(str_path_file+'/'+'Generators.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(1, n_gen+1):
            writer.writerow(['G%s' % i])
        if flag_VPE:
            for i in range(n_gen_VPE):
                writer.writerow(['G%s' % (i+1+n_gen)] + list(gencost_VPE[i, 0:5]))

    with open(str_path_file+'/'+'GeneratorsCostFun.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_gen):
            writer.writerow(['G%s' % (i+1)] + list(gencost[i, 4:7])
                            + [0, 0])  # Last two zeros because no VPE here
        if flag_VPE:
            for i in range(0, n_gen_VPE):
                writer.writerow(['G%s' % (i+1+n_gen)] + list(gencost_VPE[i, 0:5]))  # VPE here


    with open(str_path_file+'/'+'Lines.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(1, n_line+1):
            writer.writerow(['L%s' % i])

    with open(str_path_file+'/'+'Loads.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(1, n_bus+1):
            writer.writerow(['LD%s' % i])

    with open(str_path_file+'/'+'MaxRunCapacity.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_gen):
            writer.writerow(['G%s' % (i+1), gen[i, 8]])
        if flag_VPE:
            for i in range(0, n_gen_VPE):
                writer.writerow(['G%s' % (i+1+n_gen), gencost_VPE[i, 6]])

    with open(str_path_file+'/'+'MinRunCapacity.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_gen):
            writer.writerow(['G%s' % (i+1), gen[i, 9]])
        if flag_VPE:
            for i in range(0, n_gen_VPE):
                writer.writerow(['G%s' % (i+1+n_gen), gencost_VPE[i, 5]])

    with open(str_path_file+'/'+'RampDown.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_gen):
            # Ramp set at 10% of P_max - P_min
            writer.writerow(['G%s' % (i+1), 0.1*(gen[i, 8] - gen[i, 9])])
        if flag_VPE:
            for i in range(0, n_gen_VPE):
                writer.writerow(['G%s' % (i+1+n_gen), gencost_VPE[i, 7]])

    with open(str_path_file+'/'+'RampUp.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_gen):
            # Ramp set at 10% of P_max - P_min
            writer.writerow(['G%s' % (i+1), 0.1*(gen[i, 8] - gen[i, 9])])
        if flag_VPE:
            for i in range(0, n_gen_VPE):
                writer.writerow(['G%s' % (i+1+n_gen), gencost_VPE[i, 8]])

    with open(str_path_file+'/'+'Susceptance.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_line):
            if branch[i, 8] == 0:  # Branch
                writer.writerow(['L%s' % (i+1), 1.0/branch[i, 3]])
            else:  # Transformer
                writer.writerow(['L%s' % (i+1), 1.0/branch[i, 3]/branch[i, 8]])

    with open(str_path_file+'/'+'TC.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_line):
            writer.writerow(['L%s' % (i+1), branch[i, 5]])

    createTimeStep(T)
    
    with open(str_path_file+'/'+'ToBus.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_line):
            writer.writerow(['L%s' % (i+1), 'B%s' % int(branch[i, 1])])
def fDemand(D, nbr_Period):
    t = np.linspace(0, 2*math.pi, nbr_Period)
    demand = list(1.3*D*(1-0.25*np.sin(t))) # TODO  1.3  and - 0.25
    # plt.plot(t, demand)
    # plt.show()
    return demand

def createDemand(n_bus, bus_demand, T, path_file=str_path_file):
    print(n_bus)
    with open(path_file+'/'+'Demand.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_bus):
            if hasattr(bus_demand, "__len__"):
                mean_D = max(bus_demand[i, 2], 0)  #TODO Because there are dispatchable load
            else:
                mean_D = bus_demand
            D = fDemand(mean_D, T)
            writer.writerow(['LD%s' % (i+1)] + D)

def createTimeStep(T):
    with open(str_path_file+'/'+'TimeStep.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for t in range(0, T):
            writer.writerow(['T%s' % (t+1)])


def str_to_bool(s):
    if s == 'True':
        return True
    elif s == 'False':
        return False
    else:
        raise ValueError('Invalid argument, should be "True" or "False"') # evil ValueError that doesn't tell you what the wrong value was


def main(argv):
    print("Launching script", argv[0])
    print("with argument", argv[1:])

    if len(argv) != 9:
        print("Invalide set of parameter launching default test")
        flag_VPE = True
        flag_multiple_generator = True
        T = 8
        RU = 15.0
        RD = 15.0
        writeDataMatpower(T, RD, RU, flag_VPE, flag_multiple_generator)
    elif argv[1] == 'matpower':
        os.system('cp '+ str_path_data+'TC_nonzero.csv '+str_path_file)
        np.random.seed(int(argv[7]))
        writeDataMatpower(int(argv[2]), float(argv[3]), float(argv[4]), str_to_bool(argv[5]), str_to_bool(argv[6]))
    elif argv[1].split('_')[0] == 'data':
        str_path_data_i = os.path.join(str_path_data, argv[1])
        os.system('cp ' + os.path.join(str_path_data_i, 'Bloss_original.csv') +  ' ' + os.path.join(str_path_data_i, 'Bloss.csv'))
        os.system('cp '+ str_path_data_i +'/* '+str_path_file)
        if str_to_bool(argv[8]):
            D=1*np.loadtxt(os.path.join(str_path_data_i,'initial_demand.txt'))        
            createDemand(1, D, int(argv[2]))
        else:
            os.system('cp '+os.path.join(str_path_data, argv[1], 'Demand.csv ')+ str_path_file)

        createTimeStep(int(argv[2]))
    elif argv[1].split('_')[0] == 'VPE':
        print("Duplicating VPE")
        arg1 = argv[1].split('_')
        
        str_path_data_i = os.path.join(str_path_data, "_".join(arg1[2:]) )
        if str_to_bool(argv[8]):
            D=1*np.loadtxt(os.path.join(str_path_data_i,'initial_demand.txt')) 
            print('D=', D, int(argv[2]))
            createDemand(1, D, int(argv[2]), str_path_data_i)
        print(str_path_data_i)
        
        # Step 1 duplicate vpe
        print(arg1[1])
        str_gen_cost = duplicate_vpe.write_vpe_data(int(arg1[1]))

        # Step 2.1 increase Bloss
        print("Updating Bloss")
        Bloss = np.loadtxt(os.path.join(str_path_data_i, 'Bloss_original.csv'), delimiter=',')
        n_gen, _ = Bloss.shape
        n_gen_no_VPE, _ = Bloss.shape
        gen_cost = np.loadtxt(str_gen_cost, delimiter=',')
        (n_gen_VPE, _) = gen_cost.shape
        new_Bloss = Bloss
        print('Bloss original is', Bloss)
        print('Bloss original sape is', Bloss.shape)
        print("#VPE unit = %s", n_gen)
        random.seed(0)
        add_gen = []
        for g in range(n_gen_VPE):
            i = random.randint(0, n_gen-1)
            add_gen.append(i)
            Bloss = np.vstack((Bloss, Bloss[i, :]))
            Bi = Bloss[:, i]
            n_Bi = Bi.shape[0]
            Bi = np.reshape(Bi, (n_Bi, 1))
            Bloss = np.hstack((Bloss, Bi))

        print("New Bloss as shape", Bloss.shape)
        np.savetxt(os.path.join(str_path_data_i, 'Bloss.csv'), Bloss, delimiter=',')
        #Step 2.2 increase Bloss_0
        Bloss_0 = np.loadtxt(os.path.join(str_path_data_i, 'Bloss_0_original.csv'), delimiter=',')
        print('Bloss_0 original is', Bloss_0)
        print('Bloss_0 original shape is', Bloss_0.shape)
        
        for i in add_gen:
            Bloss_0 = np.hstack((Bloss_0, Bloss_0[i]))
        Bloss_0 = abs(Bloss_0)


        
        np.savetxt(os.path.join(str_path_data_i, 'Bloss_0.csv'), Bloss_0, delimiter=',')
        # Remove B_loss_00
        Bloss_00 = np.array([0])
        np.savetxt(os.path.join(str_path_data_i, 'Bloss_00.csv'), Bloss_00, delimiter=',')
        # Step 2.2 increase Bloss
        if str_to_bool(argv[8]):
            D=1*np.loadtxt(os.path.join(str_path_data_i,'initial_demand.txt'))        
            createDemand(1, D, int(argv[2]))
        else:
            print("Copying demand")
            os.system('cp '+os.path.join(str_path_data_i, 'Demand.csv ')+ str_path_file)

        createTimeStep(int(argv[2]))
        print("Copying to temporary folder data/tmp")
        os.system('cp '+ str_path_data_i +'/* '+str_path_file)
        print(str_path_file)
        #TODO Use previous demand
        # Step 3 writing on file
        print(str_path_data_i)
        gen = genfromtxt(os.path.join(str_path_data_i, end_str_gen), delimiter=',')
        gencost = genfromtxt(os.path.join(str_path_data_i, end_str_gencost), delimiter=',')
        (n_gen, _) = gencost.shape

        file_GeneratorsCostFun = os.path.join(str_path_file, 'GeneratorsCostFun.csv')
        gencost_VPE = genfromtxt(str_path_VPE + end_str_gencost, delimiter=',')
        flag_VPE = True
        with open(file_GeneratorsCostFun, 'a') as csv_file:
            for g in range(n_gen_VPE):
                csvwriter = csv.writer(csv_file)  
                l = ['G'+str(g+n_gen_no_VPE+1)]
                l = l+ list(gen_cost[g,:])
                csvwriter.writerow(l) 
        with open(str_path_file+'/'+'Generators.csv', 'w') as write_file:
            writer = csv.writer(write_file)
            for i in range(1, n_gen+1):
                writer.writerow(['G%s' % i])
            if flag_VPE:
                for i in range(n_gen_VPE):
                    writer.writerow(['G%s' % (i+1+n_gen)])
        with open(str_path_file+'/'+'MaxRunCapacity.csv', 'w') as write_file:
            writer = csv.writer(write_file)
            for i in range(0, n_gen_no_VPE):
                print('i =', i)
                print(gen.shape)
                writer.writerow(['G%s' % (i+1), gen[i, 8]])
            if flag_VPE:
                for i in range(0, n_gen_VPE):
                    j = i%10
                    print('coucou', str_path_file)
                    writer.writerow(['G%s' % (i+1+n_gen), gencost_VPE[j, 6]])

        with open(str_path_file+'/'+'MinRunCapacity.csv', 'w') as write_file:
            writer = csv.writer(write_file)
            for i in range(0, n_gen):
                writer.writerow(['G%s' % (i+1), gen[i, 9]])
            if flag_VPE:
                for i in range(0, n_gen_VPE):
                    j = i%10
                    writer.writerow(['G%s' % (i+1+n_gen), gencost_VPE[j, 5]])

        with open(str_path_file+'/'+'RampDown.csv', 'w') as write_file:
            writer = csv.writer(write_file)
            for i in range(0, n_gen):
                # Ramp set at 10% of P_max - P_min
                writer.writerow(['G%s' % (i+1), 0.1*(gen[i, 8] - gen[i, 9])])
            if flag_VPE:
                for i in range(0, n_gen_VPE):
                    j = i%10
                    writer.writerow(['G%s' % (i+1+n_gen), gencost_VPE[j, 7]])

        with open(str_path_file+'/'+'RampUp.csv', 'w') as write_file:
            writer = csv.writer(write_file)
            for i in range(0, n_gen):
                # Ramp set at 10% of P_max - P_min
                writer.writerow(['G%s' % (i+1), 0.1*(gen[i, 8] - gen[i, 9])])
            if flag_VPE:
                for i in range(0, n_gen_VPE):
                    j = i%10
                    writer.writerow(['G%s' % (i+1+n_gen), gencost_VPE[j, 8]])

            # Removing Buses
            with open(str_path_file+'/'+'Buses.csv', 'w') as write_file:
                writer = csv.writer(write_file)
                writer.writerow(['B1'])
            # Removing Loads
            with open(str_path_file+'/'+'Loads.csv', 'w') as write_file:
                writer = csv.writer(write_file)
                writer.writerow(['LD1'])
    else:
        main()


def increase_csv(add_gen, file_name, n, str_path_data_i, X):
    full_file_name = 'TOTO'
    i_num = 1
    with open(file_name, 'a') as csv_file:
        csvwriter = csv.writer(csv_file)  
        for i in add_gen:
            l = 'G'+str(n+i_num)+','+str(X[i])
            i_num += 1
            #csvwriter.writerow(l) 


if __name__=="__main__":
    main(sys.argv)


