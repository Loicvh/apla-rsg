#!/usr/bin/env python3
# 
# This simple script duplicates the VPE generators in a 
# stochastic way to reduce unwanted symmetry
#
# For simplicity, it should be launch from this directory

import numpy as np
import os

str_path = "vpe"
str_init = "gencost_init.csv"

def write_vpe_data(n_dup=1):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    print("Current path is ", dir_path)
    np.random.seed(0)
    str_file = os.path.join(dir_path, str_path, str_init)
    print("File path is", str_file)
    P = np.genfromtxt(str_file, delimiter=',')
    n, m = P.shape
    print(n, m)
    n_gen = n_dup*n
    str_out = os.path.join(dir_path, str_path, "gencost_"+str(n_gen)+".csv")
    P_out = np.zeros((n*n_dup, m))
    print(P.shape)
    P_out[0:n, :] = P

    for j in range(1, n_dup):
        R = (np.random.random_sample((n, m))-0.5)/5
        _P = P + R * P
        P_out[n*j:n*(j+1), :] = _P
    np.savetxt(str_out, P_out, delimiter=',')
    return str_out

if __name__ == "__main__":
    write_vpe_data()
