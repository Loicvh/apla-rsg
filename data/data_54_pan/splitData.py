import csv
from numpy import genfromtxt
import numpy as np
import math
import matplotlib.pyplot as plt
import copy
import sys
import os


root_dir, _ = os.path.split(os.path.dirname(os.path.abspath(__file__)))
str_path = os.path.join(root_dir, 'data/matpower/')
end_str_bus = 'bus.csv'
end_str_branch = 'branch.csv'
end_str_gen = 'gen.csv'
end_str_gencost = 'gencost.csv' 
str_path_VPE = os.path.join(root_dir, 'data/vpe/')
str_path_file = os.path.join(root_dir, 'data')


def writeData(T, RD, RU, flag_VPE, flag_multiple_generator):
    bus = genfromtxt(str_path+end_str_bus, delimiter=',')
    branch = genfromtxt(str_path+end_str_branch, delimiter=',')
    gen = genfromtxt(str_path+end_str_gen, delimiter=',')
    gencost = genfromtxt(str_path+end_str_gencost, delimiter=',')

    n_bus = len(bus)
    (n_gen, _) = gencost.shape
    (n_line, _) = branch.shape

    print('Number of buses is', n_bus)
    print('Number of generators is', n_gen)
    print('Number of lines is', n_line)
    if flag_VPE:
        gencost_VPE = genfromtxt(str_path_VPE + end_str_gencost, delimiter=',')
        (n_gen_VPE, _) = gencost_VPE.shape
        print('Number of VPE generators is', n_gen_VPE)
        if flag_multiple_generator:
            bus_VPE = copy.copy(bus[:, 0])
            np.random.shuffle(bus_VPE)
            bus_VPE = bus_VPE[0:n_gen_VPE]
        else:
            bus_without_gen = np.array([x for x in bus[:, 0] if x not in gen[:, 0]])
            np.random.shuffle(bus_without_gen)
            bus_VPE = bus_without_gen[0:n_gen_VPE]


    # Creating Buses.csv
    with open(str_path_file+'/'+'Buses.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(1, n_bus+1):
            writer.writerow(['B%s' % i])

    with open(str_path_file+'/'+'BusGenerators.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_gen):
            writer.writerow(['G%s' % (i+1), 'B%s' % int(gen[i, 0])])
        if flag_VPE:
            for i in range(0, n_gen_VPE):
                writer.writerow(['G%s' % (i+1+n_gen), 'B%s' % int(bus_VPE[i])])


    # Creating BusLoads.csv
    with open(str_path_file+'/'+'BusLoads.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_bus):
            writer.writerow(['LD%s' % (i+1), 'B%s' % (i+1)])

    with open(str_path_file+'/'+'Demand.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_bus):
            mean_D = max(bus[i, 2], 0)  #TODO Because there are dispatchable load
            D = fDemand(mean_D, T)
            writer.writerow(['LD%s' % (i+1)] + D)

    with open(str_path_file+'/'+'FromBus.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_line):
            writer.writerow(['L%s' % (i+1), 'B%s' % int(branch[i, 0])])

    with open(str_path_file+'/'+'Generators.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(1, n_gen+1):
            writer.writerow(['G%s' % i])
        if flag_VPE:
            for i in range(n_gen_VPE):
                writer.writerow(['G%s' % (i+1+n_gen)] + list(gencost_VPE[i, 0:5]))

    with open(str_path_file+'/'+'GeneratorsCostFun.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_gen):
            writer.writerow(['G%s' % (i+1)] + list(gencost[i, 4:7])
                            + [0, 0])  # Last two zeros because no VPE here
        if flag_VPE:
            for i in range(0, n_gen_VPE):
                writer.writerow(['G%s' % (i+1+n_gen)] + list(gencost_VPE[i, 0:5]))  # VPE here


    with open(str_path_file+'/'+'Lines.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(1, n_line+1):
            writer.writerow(['L%s' % i])

    with open(str_path_file+'/'+'Loads.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(1, n_bus+1):
            writer.writerow(['LD%s' % i])

    with open(str_path_file+'/'+'MaxRunCapacity.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_gen):
            writer.writerow(['G%s' % (i+1), gen[i, 8]])
        if flag_VPE:
            for i in range(0, n_gen_VPE):
                writer.writerow(['G%s' % (i+1+n_gen), gencost_VPE[i, 6]])

    with open(str_path_file+'/'+'MinRunCapacity.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_gen):
            writer.writerow(['G%s' % (i+1), gen[i, 9]])
        if flag_VPE:
            for i in range(0, n_gen_VPE):
                writer.writerow(['G%s' % (i+1+n_gen), gencost_VPE[i, 5]])

    with open(str_path_file+'/'+'RampDown.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_gen):
            # Ramp set at 10% of P_max - P_min
            writer.writerow(['G%s' % (i+1), 0.1*(gen[i, 8] - gen[i, 9])])
        if flag_VPE:
            for i in range(0, n_gen_VPE):
                writer.writerow(['G%s' % (i+1+n_gen), gencost_VPE[i, 7]])

    with open(str_path_file+'/'+'RampUp.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_gen):
            # Ramp set at 10% of P_max - P_min
            writer.writerow(['G%s' % (i+1), 0.1*(gen[i, 8] - gen[i, 9])])
        if flag_VPE:
            for i in range(0, n_gen_VPE):
                writer.writerow(['G%s' % (i+1+n_gen), gencost_VPE[i, 8]])

    with open(str_path_file+'/'+'Susceptance.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_line):
            if branch[i, 8] == 0:  # Branch
                writer.writerow(['L%s' % (i+1), 1.0/branch[i, 3]])
            else:  # Transformer
                writer.writerow(['L%s' % (i+1), 1.0/branch[i, 3]/branch[i, 8]])

    with open(str_path_file+'/'+'TC.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_line):
            writer.writerow(['L%s' % (i+1), branch[i, 5]])

    with open(str_path_file+'/'+'TimeStep.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for t in range(0, T):
            writer.writerow(['T%s' % (t+1)])

    with open(str_path_file+'/'+'ToBus.csv', 'w') as write_file:
        writer = csv.writer(write_file)
        for i in range(0, n_line):
            writer.writerow(['L%s' % (i+1), 'B%s' % int(branch[i, 1])])

def fDemand(D, nbr_Period):
    t = np.linspace(0, 2*math.pi, nbr_Period)
    demand = list(1*D*(1-0.25*np.sin(t))) # TODO  1.3  and - 0.25
    # plt.plot(t, demand)
    # plt.show()
    return demand


def str_to_bool(s):
    if s == 'True':
        return True
    elif s == 'False':
        return False
    else:
        raise ValueError('Invalid argument, should be "True" or "False"') # evil ValueError that doesn't tell you what the wrong value was


def main(argv):
    print("Launching script", argv[0], "with argument", argv[1:])
    if len(argv) != 7:
        print("Invalid set of parameter launching default test")
        flag_VPE = True
        flag_multiple_generator = True
        T = 8
        RU = 15.0
        RD = 15.0
        writeData(T, RD, RU, flag_VPE, flag_multiple_generator)
    else:
        np.random.seed(int(argv[6]))
        writeData(int(argv[1]), float(argv[2]), float(argv[3]), str_to_bool(argv[4]), str_to_bool(argv[5]))

if __name__=="__main__":
    main(sys.argv)


