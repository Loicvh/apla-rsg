function [B B0 B00 PL PLV]=KronCoeff(caso)

% Kron Coefficents (loss coefficients) for power systems using matpower (please note that matpower must be in you path)
% Code developed to calculate the kron coefficents (in order to get later robust 
% loss coefficients) mentioned in the following paper
% 'Robust Loss Coefficients: Application to Power Systems with Solar and Wind Energy'   
% by Dario Arango, Ricardo Urrego, Sergio Rivera (srriverar@unal.edu.co)
% in International Journal of Power and Energy Conversion 
%
% [B B0 B00 PL PLV]=kron(caso)
%
%   caso must have consecutive bus numbering (i.e. internal bus numbering)
%
%   LOSSES = (Pgen'*(B)*Pgen +B0*Pgen+B00)*Sb;
%
%   Example:
%         [B B0 B00 PL PLV]=KronCoeff(case30)
%
%         results=runpf('case30');
%         Sb=results.baseMVA;
%         Pgen=results.gen(:,2)/Sb;
%         
%         LOSSES_KronCoefficients = (Pgen'*(B)*Pgen +B0*Pgen+B00)*Sb % It
%         %must be equal to PL
%         
%         LOSSES_PowerFlow=sum(results.branch(:,14)+results.branch(:,16))
%         %It mus be equal to PLV
%
% If you use the code please you can cite the paper in the following way:
% Arango, D.; Urrego, R.; Rivera, S. (in press). Robust Loss Coefficients:
% Application to Power Systems with Solar and Wind Energy. International
% Journal of Power and Energy Conversion. 2017



Sb=caso.baseMVA;
% se corre el flujo de carga del caso
resultados=runpf(caso);
%se calcula la matriz de admitancias
Yb=makeYbus(caso);
%se calcula la inversa qeu es la matriz de impedancias
Zb=full(inv(Yb));
% se obtiene su parte real
Rb=real(Zb);
% Magnitud de las tensiones, angulos, 
%potencias generadas PGEN QGEN
%Potencia demandadas Pdem Qdem
Pgen=resultados.gen(:,2)/Sb;
Qgen=resultados.gen(:,3)/Sb;
Pdem=resultados.bus(:,3)/Sb;
Qdem=resultados.bus(:,4)/Sb;
v=resultados.bus(:,8);
PLV=sum(resultados.branch(:,14)+resultados.branch(:,16)); %LOSSES_PowerFlow
%Los angulos se pasan a radianes
teta=resultados.bus(:,9)*(pi/180);
%tensiones complejas
V=v.*exp(1i.*teta);
[m c]=size(Pgen);
[N c]=size(V);
%obtener slack
for k=1:1:N
    if resultados.bus(k,2)==3 
    sl=k;
    k=N;
    end
end
cal=[resultados.gen(:,1) resultados.gen(:,2) resultados.gen(:,3)];
%Vector con nodos de generación bajo el criterio de PG 
% es diferennte de cero
%Pgen(Pgen==0)=0.000001/Sb; CHANGED BY LVH
nodosgen=cal(:,1);
VGEN=V(nodosgen);
% Calculo de corrientes generadas
IG=(Pgen-1i*Qgen)./(conj(VGEN));
%Calculo de corrientes demandadas
ILK=(Pdem-1i*Qdem)./(conj(V));
%Valores 
ID=sum(ILK);
LK=ILK/ID;
T=Zb(sl,:)*LK;
% creacion matriz C
Zclave=[Zb(sl,nodosgen) Zb(sl,sl)];
 C1=-(LK/T)*Zclave;
 %vector ascendente
 clave=[1:1:m]';
 for i=1:1:m
        C1(nodosgen(i,1),clave(i,1))=1+C1(nodosgen(i,1),clave(i,1));  
 end
 
 
C=C1; % creación de matriz PHI;
dim=m+1;
PHI=zeros(1, 1);
for k=1:1:m
   F=1i*(Qgen(k,1)/(Pgen(k,1)));
   PHI(k,k)=(1-F)/(conj(VGEN(k,1)));
end

PHI(abs(PHI)==Inf) == 0 % ADDED BY LVH

IO=-1*V(sl,1)/(Zb(sl,sl));
PHI(dim,dim)=IO;
% Hasta acá todo esta bien
IGB=[IG;IO];

 H=real(PHI*conj(C)'*real(Zb)*conj(C)*conj(PHI));


B=H(1:m,1:m);     % Partición de la matriz H conforme ecuación [36].
B0=2*H(m+1,1:m);
B00=H(m+1,m+1);

Pgen

PL=(Pgen'*(B)*Pgen +B0*Pgen+B00)*Sb; %LOSSES_KronCoefficients

