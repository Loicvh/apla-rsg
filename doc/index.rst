.. Histograms time series prediction documentation master file, created by
   sphinx-quickstart on Tue May  5 11:56:08 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to APLA-RSG documentation!
=============================================================

.. toctree::
   :maxdepth: 3
   :caption: Table of Contents
   
   usage/installation
   usage/quickstart
   usage/functions

