.. toctree::
   :maxdepth: 2

INSTALLATION
============


Simply clone the repository on your computer.

``git clone https://gitlab.com/Loicvh/apla-rsg``

Dependencies
____________

This repository depends on

- Multiple python libraries: numpy, csv, os, ...

- Python Gurobi API, install it https://www.gurobi.com/documentation/8.1/refman/py_python_api_overview.html. Free academic license can be obtained on their website.

- Julia (JuMP) with Ipopt.

