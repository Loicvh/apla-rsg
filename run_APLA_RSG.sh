#!/bin/sh

SCRIPT=$(readlink -f $0)
DIRECTORY=`dirname $SCRIPT`
DATA_TYPE="data_5_bis"  # Don't forget to switch last argument as TRUE
DATA_TYPE="data_54"
DATA_TYPE="data_15"  # createDemand = True
DATA_TYPE="data_10"
DATA_TYPE="data_5"  # createDemand = False


T=24

echo ${DIRECTORY}
find $DIRECTORY/tmp/ -type f -name "*.txt" -delete
find $DIRECTORY/data/tmp/ -type f -name "*.csv" -delete

python3 "$DIRECTORY/data/moveData.py" ${DATA_TYPE} ${T} 0 0 True True 1 False # T RD RU flag_VPE flag_multiple seed flag createDemand
python3 "$DIRECTORY/samples/test_eldp.py"

#cp tmp/sol* $DIRECTORY/ELDP_ramp/DATA
