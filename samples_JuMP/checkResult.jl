



println("Checking result")



function f(x)
	let out = 0
		for t in 1:n_T, g in 1:n_G
			p_tg = value(p[t, g])
			out += df[g, :A] * p_tg^2 + df[g, :B]*p_tg + df[g, :C] + df[g, :D]*abs(sin(df[g, :E]*(p_tg-df[g, :P_min])))
		end
	return out
	end
end

println("True objective is ", f(p))

total_dev = 0
let dev = 0, losses = 0
	for t in 1:n_T
		dev = dev + abs(value(sum_gen[t])  - D[t, :D] - value(p_loss[t]))
		losses = losses + value(p_loss[t])

	end

	println("Balance deviation ", dev)
	println("Losses ", losses)
end

function check_box(x)
	let flag_box = true, count = 0
		for t in 1:n_T, g in 1:n_G
			p_tg = value(p[t, g])
			if p_tg > df[g, :"P_max"] || p_tg < df[g, :"P_min"]
				count = count + 1
				flag_box = false
			end

		end
	if !flag_box
		println("Number of power range violation = ", count)
	else 
		println("No box violation")
	end
	end
end

println("Checking box")
check_box(p)

function check_ramp(x)
	let flag_ramp = true, count = 0
		for t in 2:n_T, g in 1:n_G
			p_tg = value(p[t, g])
			p_prev = value(p[t-1, g])
			if p_tg - p_prev  > df[g, :"R_up"] || -df[g, :"R_down"] > p_tg - p_prev
				count = count + 1
				flag_ramp = false
			end

		end
	if !flag_ramp
		println("Number of ramp violation = ", count)
	else 
		println("No ramp violation")
	end
	end
end

println("Checking ramp")
check_ramp(p)
